package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.Exceptions.PlanException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan {
    private UUID id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;

    public Plan() {

    }

    public Plan (String name, String details, int maxSeats, int minSeats, int feePerUnit) throws PlanException {
        this.id = UUID.randomUUID();
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;

        validateName(name);
        validateDetails(details);
        validateMaxSeats(maxSeats);
        validateMinSeats(minSeats, maxSeats);
        validateFeePerUnit(feePerUnit);
    }

    public void validateName(String name) throws PlanException {
        if (name == null) {
            throw new PlanException("Name is null");
        }
        if (name.length() < 2) {
            throw new PlanException("Name is too short");
        }
        if (name.length() > 128) {
            throw new PlanException("Name is too long");
        }
        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i)))
                throw new PlanException("Name contains special symbols");
        }
    }

    public void validateDetails(String details) throws PlanException {
        if (details == null) {
            throw new PlanException("Details are null");
        }
        if (details.length() < 1) {
            throw new PlanException("Details are too short");
        }
        if (details.length() > 1024) {
            throw new PlanException("Details are too long");
        }
    }

    public void validateMaxSeats(int maxSeats) throws PlanException {
        if (maxSeats < 1) {
            throw new PlanException("MaxSeats value too small");
        }
        if (maxSeats > 999999) {
            throw new PlanException("MaxSeats value too big");
        }
    }

    public void validateMinSeats(int minSeats, int maxSeats) throws PlanException {

        if (minSeats < 1) {
            throw new PlanException("MinSeats value too small");
        }
        if (minSeats > 999999) {
            throw new PlanException("MinSeats value too big");
        }
        if (minSeats > maxSeats) {
            throw new PlanException("MinSeats bigger than MaxSeats");
        }
    }

    public void validateFeePerUnit(int feePerUnit) throws PlanException{
        if (feePerUnit < 0) {
            throw new PlanException("feePerUnit too small");
        }
        if (feePerUnit > 999999) {
            throw new PlanException("feePerUnit too big");
        }
    }
}
