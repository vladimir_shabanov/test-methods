package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.database.Exceptions.UserException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;


    public User(String firstName, String lastName, String login, String pass) throws UserException {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;

        validateName(firstName, "First");
        validateName(lastName, "Last");
        validateLogin(login);
        validatePassword(pass, login, firstName, lastName);
    }

    public void validateName(String name, String type) throws UserException {
        if (name == null)
            throw new UserException(type + "name is null");
        if (name.length() > 12)
            throw new UserException(type + "name is too long");
        if (name.length() < 2)
            throw new UserException(type + "name is too short");
        if (name.contains(" "))
            throw new UserException(type + "name contains space char");
        if (name.charAt(0) < 'A' || name.charAt(0) > 'Z')
            throw new UserException(type + "name starts with incorrect symbol");
        for (int i = 1; i < name.length(); i++) {
            if (name.charAt(i) < 'a' || name.charAt(i) > 'z')
                throw new UserException(type + "name contains incorrect symbols");
        }
    }

    public void validateLogin(String login) throws UserException {
        if (login == null)
            throw new UserException("Login is null");
        if (!login.contains("@"))
            throw new UserException("Invalid login");
        if (!login.contains("."))
            throw new UserException("Invalid login");
    }

    public void validatePassword(String password, String login, String firstName, String lastName) throws UserException {
        if (password == null)
            throw new UserException("Password is null");
        if (password.length() < 6)
            throw new UserException("Password is too short");
        if (password.length() > 12)
            throw new UserException("Password is too long");
        if (password.contains(firstName) || password.contains(lastName) || password.contains(login.substring(0, login.indexOf('@'))))
            throw new UserException("Password is insecure");
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }
}
