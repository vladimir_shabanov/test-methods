package ru.nsu.fit.endpoint.service.databse.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.Exceptions.UserException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() throws Exception {
        new User("John", "Wick", "john_wick@gmail.com", "strongpass");
    }

    @Test
    public void testCreateNewCustomerWithShortPass() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is too short");
        new User("John", "Wick", "john_wick@gmail.com", "12345");
    }

    @Test
    public void testCreateNewCustomerWithShortPass1() throws UserException {
        new User("John", "Wick", "john_wick@gmail.com", "123456");
    }

    @Test
    public void testCreateNewCustomerWithLongPass() throws UserException {
        new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe");
    }

    @Test
    public void testCreateNewCustomerWithLongPass1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is too long");
        new User("John", "Wick", "john_wick@gmail.com", "123qwe123qwe1");
    }

    @Test
    public void testCreateNewCustomerWithNullPass() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is null");
        new User("John", "Wick", "john_wick@gmail.com", null);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User("John", "Wick", "john_wick@gmail.com", "Johnie");
    }

    @Test
    public void testCreateNewCustomerWithEasyPass2() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User("John", "Wick", "john_wick@gmail.com", "Wickie");
    }

    @Test
    public void testCreateNewCustomerWithEasyPass3() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Password is insecure");
        new User("John", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname is too short");
        new User("J", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortFirstname1() throws UserException {
        new User("Jo", "Wick", "john_wick@gmail.com", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname1() throws UserException {
        new User("Johnjohnjohn", "Wick", "john_wick@gmail.com", "jon_wik");
    }

    @Test
    public void testCreateNewCustomerWithLongFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname is too long");
        new User("Johnjohnjohnj", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithSpaceInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname contains space char");
        new User("Joh n", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname starts with incorrect symbol");
        new User("john", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInFirstname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Firstname contains incorrect symbols");
        new User("JOHN", "Wick", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname is too short");
        new User("John", "W", "john_wick@gmail.com", "john_wick");
    }

    @Test
    public void testCreateNewCustomerWithShortLastname1() throws UserException {
        new User("John", "Wi", "john_wick@gmail.com", "joen_wiwk");
    }

    @Test
    public void testCreateNewCustomerWithLongLastname1() throws UserException {
        new User("John", "Wickwickwick", "john_wick@gmail.com", "jodhn_weick");
    }

    @Test
    public void testCreateNewCustomerWithLongLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname is too long");
        new User("John", "Wickwickwick1", "john_wick@gmail.com", "johg_wieck");
    }

    @Test
    public void testCreateNewCustomerWithSpaceInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname contains space char");
        new User("John", "Wic k", "john_wick@gmail.com", "joghn_wigck");
    }

    @Test
    public void testCreateNewCustomerWithSmallFirstLetterInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname starts with incorrect symbol");
        new User("John", "wick", "john_wick@gmail.com", "johgn_wigck");
    }

    @Test
    public void testCreateNewCustomerWithBigLettersInLastname() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Lastname contains incorrect symbols");
        new User("John", "WICK", "john_wick@gmail.com", "johgn_wicgk");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Invalid login");
        new User("John", "Wick", "john_wickgmail.com", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin1() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Invalid login");
        new User("John", "Wick", "john_wick@gmailcom", "jon_wck");
    }

    @Test
    public void testCreateNewCustomerIncorrectLogin2() throws UserException {
        expectedEx.expect(UserException.class);
        expectedEx.expectMessage("Login is null");
        new User("John", "Wick", null, "jon_wck");
    }


}
