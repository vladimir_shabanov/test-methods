package ru.nsu.fit.endpoint.service.databse.data;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.Exceptions.PlanException;
import ru.nsu.fit.endpoint.service.database.data.Plan;

/**
 * Created by Vladimir on 24.10.2016.
 */
public class PlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewPlan() throws Exception {
        new Plan("hello", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanNullName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is null");
        new Plan(null, "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is too short");
        new Plan("P", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortName1() throws Exception {
        new Plan("Pl", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanLongName() throws Exception {
        new Plan("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefgh", "detail", 333, 222, 1000);
    }

    public void testCreateNewPlanLongName1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name is too long");
        new Plan("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghA", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanWrongName() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Name contains special symbols");
        new Plan("P111", "detail", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanNullDetails() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Details are null");
        new Plan("hello", null, 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortDetails() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("Details are too short");
        new Plan("hello", "", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanShortDetails1() throws Exception {
        new Plan("hello", "a", 333, 222, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeats() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MaxSeats value too small");
        new Plan("hello", "a", 0, 222, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMaxSeats1() throws Exception {
        new Plan("hello", "a", 1, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMaxSeats() throws Exception {
        new Plan("hello", "a", 999999, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMaxSeats1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MaxSeats value too big");
        new Plan("hello", "a", 1000000, 1, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMinSeats() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MinSeats value too small");
        new Plan("hello", "a", 111, 0, 1000);
    }

    @Test
    public void testCreateNewPlanSmallMinSeats1() throws Exception {
        new Plan("hello", "a", 111, 1, 1000);
    }

    @Test
    public void testCreateNewPlanBigMinSeats() throws Exception {
        new Plan("hello", "a", 999999, 999999, 1000);
    }

    @Test
    public void testCreateNewPlanBigMinSeats1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MinSeats value too big");
        new Plan("hello", "a", 100000, 1000000, 1000);
    }

    @Test
    public void testCreateNewPlanMinMaxSeats() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("MinSeats bigger than MaxSeats");
        new Plan("hello", "a", 100000, 100001, 1000);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnit() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("feePerUnit too small");
        new Plan("hello", "detail", 333, 222, -1);
    }

    @Test
    public void testCreateNewPlanSmallFeePerUnit1() throws Exception {
        new Plan("hello", "detail", 333, 222, 0);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnit() throws Exception {
        new Plan("hello", "detail", 333, 222, 999999);
    }

    @Test
    public void testCreateNewPlanBigFeePerUnit1() throws Exception {
        expectedEx.expect(PlanException.class);
        expectedEx.expectMessage("feePerUnit too big");
        new Plan("hello", "detail", 333, 222, 1000000);
    }

}
